fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);

    Highcharts.chart("container1", {
      title: {
        text: "Matches-Played-Per-Season",
        align: "center",
      },

      yAxis: {
        title: {
          text: "Matches",
        },
      },

      xAxis: {
        title: {
          text: "IPL-Season",
        },
        accessibility: {
          rangeDescription: "Range: 2010 to 2020",
        },
      },

      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "middle",
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
          pointStart: 2008,
        },
      },

      series: [
        {
          name: "Matches",
          data: dataToPlot,
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: "horizontal",
                align: "center",
                verticalAlign: "bottom",
              },
            },
          },
        ],
      },
    });
  });

fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const rangeSeason = Object.keys(data).length;
    const dataArray = Object.entries(data);
    const dataKeys = Object.keys(data);

    let matchesWonByTeam = {};
    let index;
    dataArray.map((item) => {
      const teamsData = Object.entries(item[1]);

      teamsData.map((entry) => {
        if (matchesWonByTeam[entry[0]]) {
          index = dataKeys.indexOf(item[0]);
          matchesWonByTeam[entry[0]][index] = entry[1];
        } else {
          matchesWonByTeam[entry[0]] = [];
          matchesWonByTeam[entry[0]].length = rangeSeason;
          matchesWonByTeam[entry[0]].fill(null);

          index = dataKeys.indexOf(item[0]);
          matchesWonByTeam[entry[0]][index] = entry[1];
        }
      });
    });

    const dataToPlot = [];
    Object.keys(matchesWonByTeam).map((item) => {
      let element = {};
      element["name"] = item;
      element["data"] = matchesWonByTeam[item];

      dataToPlot.push(element);
    });

    Highcharts.chart("container2", {
      chart: {
        type: "column",
      },
      title: {
        text: "Matches-Won-Per-Team",
      },
      xAxis: {
        title: {
          text: "Season",
        },
        categories: dataKeys,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Matches Won",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: dataToPlot,
    });
  });

fetch("./output/3-extra-runs-conceded-per-team-2016.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    const teamsList = Object.keys(data);

    Highcharts.chart("container3", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra-Runs-Conceded-Per-Team",
      },
      xAxis: {
        title: {
          text: "Teams",
        },
        categories: teamsList,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Extra-Runs-Conceded",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Extra-Runs",
          data: dataToPlot,
        },
      ],
    });
  });

fetch("./output/4-top-economical-bowlers-2015.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    const bowlersList = Object.keys(data);

    Highcharts.chart("container4", {
      chart: {
        type: "column",
      },
      title: {
        text: "Top-10-Economical-Bowlers-2015 ",
      },
      xAxis: {
        title: {
          text: "Bowlers",
        },
        categories: bowlersList,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Economy",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.2f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Economy",
          data: dataToPlot,
        },
      ],
    });
  });

fetch("./output/5-won-toss-won-match.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    const teamsList = Object.values(data);

    Highcharts.chart("container5", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number-Of-Times-Team-Won-Both-Toss-And-Match",
      },
      xAxis: {
        title: {
          text: "Teams",
        },
        categories: teamsList,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Won-Both-Toss-&-Match",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Won-Both-Toss-&-Match",
          data: dataToPlot,
        },
      ],
    });
  });

fetch("./output/6-most-times-player-of-match.json")
  .then((data) => data.json())
  .then((data) => {
    const rangeSeason = Object.keys(data).length;
    const dataArray = Object.entries(data);
    const dataKeys = Object.keys(data);

    let dataPlayerOfMatch = {};
    let index;
    dataArray.map((item) => {
      const playersData = Object.entries(item[1]);

      playersData.map((entry) => {
        if (dataPlayerOfMatch[entry[0]]) {
          index = dataKeys.indexOf(item[0]);
          dataPlayerOfMatch[entry[0]][index] = entry[1];
        } else {
          dataPlayerOfMatch[entry[0]] = [];
          dataPlayerOfMatch[entry[0]].length = rangeSeason;
          dataPlayerOfMatch[entry[0]].fill(null);

          index = dataKeys.indexOf(item[0]);
          dataPlayerOfMatch[entry[0]][index] = entry[1];
        }
      });
    });

    const dataToPlot = [];
    Object.keys(dataPlayerOfMatch).map((item) => {
      let element = {};
      element["name"] = item;
      element["data"] = dataPlayerOfMatch[item];

      dataToPlot.push(element);
    });

    Highcharts.chart("container6", {
      chart: {
        type: "column",
      },
      title: {
        text: "Highest Times: Player Of The Match",
      },
      xAxis: {
        title: {
          text: "Season",
        },
        categories: dataKeys,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Times: Player Of The Match",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: dataToPlot,
    });
  });

fetch("./output/7-strike-rate-batsman-each-season.json")
  .then((data) => data.json())
  .then((data) => {
    const rangeSeason = Object.keys(data).length;
    const dataArray = Object.entries(data);
    const dataKeys = Object.keys(data);

    let strikeRate = {};
    let index;
    dataArray.map((item) => {
      const batsmanData = Object.entries(item[1]);

      batsmanData.map((entry) => {
        if (strikeRate[entry[0]]) {
          index = dataKeys.indexOf(item[0]);
          strikeRate[entry[0]][index] = entry[1];
        } else {
          strikeRate[entry[0]] = [];
          strikeRate[entry[0]].length = rangeSeason;
          strikeRate[entry[0]].fill(0);

          index = dataKeys.indexOf(item[0]);
          strikeRate[entry[0]][index] = entry[1];
        }
      });
    });

    const dataToPlot = [];
    Object.keys(strikeRate).map((item) => {
      let element = {};
      element["name"] = item;
      element["data"] = strikeRate[item];

      dataToPlot.push(element);
    });

    Highcharts.chart("container7", {
      chart: {
        type: "column",
      },
      title: {
        text: "Strike-Rate-Batsman-All-Seasons",
      },
      xAxis: {
        title: {
          text: "IPL Season",
        },
        categories: dataKeys,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Strike-Rate",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: dataToPlot,
    });
  });

fetch("./output/8-highest-dismissal.json")
  .then((data) => data.json())
  .then((data) => {
    const rangeSeason = Object.keys(data).length;
    const dataArray = Object.entries(data);
    const dataKeys = Object.keys(data);

    let dismissalData = {};
    let index;
    dataArray.map((item) => {
      const playersData = Object.entries(item[1]);

      playersData.map((entry) => {
        if (dismissalData[entry[0]]) {
          index = dataKeys.indexOf(item[0]);
          dismissalData[entry[0]][index] = entry[1];
        } else {
          dismissalData[entry[0]] = [];
          dismissalData[entry[0]].length = rangeSeason;
          dismissalData[entry[0]].fill(null);

          index = dataKeys.indexOf(item[0]);
          dismissalData[entry[0]][index] = entry[1];
        }
      });
    });

    const dataToPlot = [];
    Object.keys(dismissalData).map((item) => {
      let element = {};
      element["name"] = item;
      element["data"] = dismissalData[item];

      dataToPlot.push(element);
    });

    Highcharts.chart("container8", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number Of Times One Player Has Been Dismissed By Another Player",
      },
      xAxis: {
        title: {
          text: "Players",
        },
        categories: dataKeys,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Dismissals",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: dataToPlot,
    });
  });

fetch("./output/9-bowler-best-economy-super-over.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    const bowler = Object.keys(data);

    Highcharts.chart("container9", {
      title: {
        text: "Best-Economy-Bowler-Super-Overs",
        align: "center",
      },

      yAxis: {
        title: {
          text: "Economy",
        },
      },

      xAxis: {
        title: {
          text: "Bowler",
        },
        categories: bowler,
        crosshair: true,
      },

      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "middle",
      },

      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },

      series: [
        {
          name: "Economy",
          data: dataToPlot,
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: "horizontal",
                align: "center",
                verticalAlign: "bottom",
              },
            },
          },
        ],
      },
    });
  });
