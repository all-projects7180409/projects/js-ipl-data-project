const csv = require("csv-parser");
const fs = require("fs");

const parsedData = [];

function matchesWonPerTeamPerYear(parsedData) {
  let result = {};

  for (let index = 0; index < parsedData.length; index++) {
    let season = parsedData[index].season;
    let winner = parsedData[index].winner;

    if (parsedData[index].winner.length > 1) {
      if (result[season]) {
        if (result[season][winner]) {
          result[season][winner]++;
        } else {
          result[season][winner] = 1;
        }
      } else {
        result[season] = {};
        result[season][winner] = 1;
      }
    }
  }

  return result;
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedData.push(data))
  .on("end", () => {
    let result = matchesWonPerTeamPerYear(parsedData);

    const content = JSON.stringify(result);

    fs.writeFile(
      "../public/output/2-matches-won-per-team-per-year.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
