const csv = require("csv-parser");
const fs = require("fs");

const parsedDataMatches = [];
const parsedDataDelivery = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedDataMatches.push(data))
  .on("end", () => {
    let seasonMatchId = {};
    let arrId = [];
    let season;

    for (let index = 0; index < parsedDataMatches.length; index++) {
      season = parsedDataMatches[index].season;
      if (seasonMatchId[season]) {
        arrId.push(parsedDataMatches[index].id);
        seasonMatchId[season] = arrId;
      } else {
        seasonMatchId[season] = {};
        arrId = [];
        arrId.push(parsedDataMatches[index].id);
        seasonMatchId[season] = arrId.push(parsedDataMatches[index].id);
      }
    }

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())
      .on("data", (data) => parsedDataDelivery.push(data))
      .on("end", () => {
        let result = {};
        let seasonMatchIdArr = Object.entries(seasonMatchId);

        for (let index = 0; index < parsedDataDelivery.length; index++) {
          let matchId = parsedDataDelivery[index].match_id;
          let currentBatsman = parsedDataDelivery[index].batsman;
          let currentBatsmanRuns = parsedDataDelivery[index].batsman_runs;

          for (let item of seasonMatchIdArr) {
            if (item[1].includes(matchId)) {
              if (result.hasOwnProperty(item[0])) {
                if (result[item[0]].hasOwnProperty(currentBatsman)) {
                  result[item[0]][currentBatsman][0] =
                    result[item[0]][currentBatsman][0] +
                    Number(currentBatsmanRuns);
                  if (!Number(parsedDataDelivery[index].wide_runs)) {
                    result[item[0]][currentBatsman][1]++;
                  }
                } else {
                  result[item[0]][currentBatsman] = [];
                  result[item[0]][currentBatsman][0] = Number(
                    parsedDataDelivery[index].batsman_runs
                  );
                  if (Number(parsedDataDelivery[index].wide_runs)) {
                    result[item[0]][currentBatsman][1] = 0;
                  } else {
                    result[item[0]][currentBatsman][1] = 1;
                  }
                }
              } else {
                result[item[0]] = {};
              }
            }
          }
        }

        Object.values(result).map((strikeRateData) => {
          Object.keys(strikeRateData).map((strikeRateDataValues) => {
            let strikeRate =
              (strikeRateData[strikeRateDataValues][0] * 100) /
              strikeRateData[strikeRateDataValues][1];
            strikeRateData[strikeRateDataValues] = strikeRate;
          });
        });

        const content = JSON.stringify(result);

        fs.writeFile(
          "../public/output/7-strike-rate-batsman-each-season.json",
          content,
          function (err) {
            if (err) {
              console.error(err);
            }
          }
        );
      });
  });
