const csv = require("csv-parser");
const fs = require("fs");

const parsedDataDelivery = [];

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv())
  .on("data", (data) => parsedDataDelivery.push(data))
  .on("end", () => {
    let result = {};

    for (let index = 0; index < parsedDataDelivery.length; index++) {
      let currentValueDelivery = parsedDataDelivery[index];
      let currentBowler = parsedDataDelivery[index].bowler;

      if (Number(currentValueDelivery.is_super_over)) {
        if (result[currentBowler]) {
          result[currentBowler][0] =
            Number(result[currentBowler][0]) +
            Number(currentValueDelivery.total_runs);
          result[currentBowler][1]++;
        } else {
          result[currentBowler] = [];
          result[currentBowler][0] = Number(currentValueDelivery.total_runs);
          result[currentBowler][1] = 1;
        }
      }
    }
    for (let item in result) {
      result[item] = result[item][0] / (result[item][1] / 6);
    }
    const sorted = Object.entries(result)
      .sort(([, a], [, b]) => a - b)
      .slice(0, 1)
      .reduce(
        (item, [key, value]) => ({
          ...item,
          [key]: value,
        }),
        {}
      );

    const content = JSON.stringify(sorted);
    fs.writeFile(
      "../public/output/9-bowler-best-economy-super-over.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
