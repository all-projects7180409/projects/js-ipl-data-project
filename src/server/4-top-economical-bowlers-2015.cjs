const csv = require("csv-parser");
const fs = require("fs");

const parsedDataMatches = [];
const parsedDataDelivery = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedDataMatches.push(data))
  .on("end", () => {
    let matchId = [];

    for (let index = 0; index < parsedDataMatches.length; index++) {
      if (parsedDataMatches[index].season === "2015") {
        matchId.push(parsedDataMatches[index].id);
      }
    }

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())
      .on("data", (data) => parsedDataDelivery.push(data))
      .on("end", () => {
        let result = {};
        let ballsCount = 0;

        for (let index = 0; index < parsedDataDelivery.length; index++) {
          let currentValueDelivery = parsedDataDelivery[index];
          let currentBowler = currentValueDelivery.bowler;
          let totalRunsPerBall = currentValueDelivery.total_runs;
          let nextBowler;

          try {
            nextBowler = parsedDataDelivery[index + 1].bowler;
          } catch (error) {
            console.error(error);
          }

          if (matchId.includes(currentValueDelivery.match_id)) {
            if (currentBowler !== nextBowler) {
              if (ballsCount < 6) {
                if (result[currentBowler]) {
                  result[currentBowler].totalRuns =
                    result[currentBowler].totalRuns + Number(totalRunsPerBall);
                  result[currentBowler].totalOvers =
                    result[currentBowler].totalOvers + ballsCount / 6;
                } else {
                  let totalRuns = Number(totalRunsPerBall);
                  let [totalOvers] = ballsCount / 6;
                  result[currentBowler] = { totalRuns, totalOvers };
                }
                ballsCount = 0;
              } else {
                result[currentBowler].totalRuns =
                  result[currentBowler].totalRuns + Number(totalRunsPerBall);
                result[currentBowler].totalOvers =
                  result[currentBowler].totalOvers + 1;
              }
            } else {
              if (result[currentBowler]) {
                result[currentBowler].totalRuns =
                  result[currentBowler].totalRuns + Number(totalRunsPerBall);
                ballsCount++;
              } else {
                let totalRuns = Number(totalRunsPerBall);
                let totalOvers = 0;
                result[currentBowler] = { totalRuns, totalOvers };
                ballsCount++;
              }
            }
          }
        }

        let economy = {};
        let resultArray = Object.entries(result);

        for (let item of resultArray) {
          economy[item[0]] = item[1].totalRuns / item[1].totalOvers;
        }

        const sorted = Object.entries(economy)
          .sort(([, item1], [, item2]) => item1 - item2)
          .slice(0, 10)
          .reduce(
            (item, [key, value]) => ({
              ...item,
              [key]: value,
            }),
            {}
          );
        const content = JSON.stringify(sorted);

        fs.writeFile(
          "../public/output/4-top-economical-bowlers-2015.json",
          content,
          function (err) {
            if (err) {
              console.error(err);
            }
          }
        );
      });
  });
