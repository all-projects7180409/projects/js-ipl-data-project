const csv = require("csv-parser");
const fs = require("fs");

const parsedData = [];

function matchesPerYear(parsedData) {
  let matchesInYear = {};

  for (let index = 0; index < parsedData.length; index++) {
    let season = parsedData[index].season;

    if (matchesInYear[season]) {
      matchesInYear[season]++;
    } else {
      matchesInYear[season] = 1;
    }
  }
  return matchesInYear;
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedData.push(data))
  .on("end", () => {
    let matchesInYear = matchesPerYear(parsedData);
    const content = JSON.stringify(matchesInYear);

    fs.writeFile(
      "../public/output/1-matches-per-year.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
