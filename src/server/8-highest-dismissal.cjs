const csv = require("csv-parser");
const fs = require("fs");

const parsedDataDelivery = [];

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv())
  .on("data", (data) => parsedDataDelivery.push(data))
  .on("end", () => {
    let result = {};

    for (let index = 0; index < parsedDataDelivery.length; index++) {
      let dismissalKind = parsedDataDelivery[index].dismissal_kind;
      let currentBatsman = parsedDataDelivery[index].batsman;
      let currentBowler = parsedDataDelivery[index].bowler;

      if (dismissalKind) {
        if (result[currentBatsman]) {
          if (result[currentBatsman][currentBowler]) {
            result[currentBatsman][currentBowler]++;
          } else {
            result[currentBatsman][currentBowler] = 1;
          }
        } else {
          result[currentBatsman] = {};
          result[currentBatsman][currentBowler] = 1;
        }
      }
    }

    let resultArray = Object.entries(result);

    for (let item of resultArray) {
      const sorted = Object.entries(item[1])
        .sort(([, item1], [, item2]) => item2 - item1)
        .slice(0, 1)
        .reduce(
          (item, [key, value]) => ({
            ...item,
            [key]: value,
          }),
          {}
        );
      result[item[0]] = sorted;
    }
    const content = JSON.stringify(result);

    fs.writeFile(
      "../public/output/8-highest-dismissal.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
