const csv = require("csv-parser");
const fs = require("fs");

const parsedData = [];

function mostTimesPlayerOfMatch(parsedData) {
  let result = {};

  for (let index = 0; index < parsedData.length; index++) {
    let season = parsedData[index].season;
    let playerOfMatch = parsedData[index].player_of_match;

    if (parsedData[index].player_of_match.length > 1) {
      if (result[season]) {
        if (result[season][playerOfMatch]) {
          result[season][playerOfMatch]++;
        } else {
          result[season][playerOfMatch] = 1;
        }
      } else {
        result[season] = {};
        result[season][playerOfMatch] = 1;
      }
    }
  }

  let resultFinal = {};
  let resultArray = Object.entries(result);

  for (let item of resultArray) {
    let max = 0;
    let maxKey = "";

    for (let entries in item[1]) {
      if (item[1][entries] > max) {
        max = item[1][entries];
        maxKey = entries;
      }
    }
    resultFinal[item[0]] = {};
    resultFinal[item[0]][maxKey] = max;
  }

  return resultFinal;
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedData.push(data))
  .on("end", () => {
    let resultFinal = mostTimesPlayerOfMatch(parsedData);

    console.log(resultFinal);
    const content = JSON.stringify(resultFinal);

    fs.writeFile(
      "../public/output/6-most-times-player-of-match.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
