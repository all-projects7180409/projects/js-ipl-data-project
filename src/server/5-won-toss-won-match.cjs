const csv = require("csv-parser");
const fs = require("fs");

const parsedData = [];

function wonTossWonMatch(parsedData) {
  let resultFinal = {};

  for (let index = 0; index < parsedData.length; index++) {
    let tossWinner = parsedData[index].toss_winner;
    let winner = parsedData[index].winner;

    if (resultFinal.hasOwnProperty(tossWinner)) {
      if (tossWinner === winner) {
        resultFinal[tossWinner] = resultFinal[tossWinner] + 1;
      }
    } else {
      resultFinal[tossWinner] = 0;
      if (tossWinner === winner) {
        resultFinal[tossWinner] = 1;
      }
    }
  }
  return resultFinal;
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedData.push(data))
  .on("end", () => {
    let resultFinal = wonTossWonMatch(parsedData);
    const content = JSON.stringify(resultFinal);

    fs.writeFile(
      "../public/output/5-won-toss-won-match.json",
      content,
      function (err) {
        if (err) {
          console.error(err);
        }
      }
    );
  });
