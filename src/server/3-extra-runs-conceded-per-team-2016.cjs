const csv = require("csv-parser");
const fs = require("fs");

const parsedDataMatches = [];
const parsedDataDelivery = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => parsedDataMatches.push(data))
  .on("end", () => {
    let matchId = [];

    for (let index = 0; index < parsedDataMatches.length; index++) {
      if (parsedDataMatches[index].season === "2016") {
        matchId.push(parsedDataMatches[index].id);
      }
    }

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())
      .on("data", (data) => parsedDataDelivery.push(data))
      .on("end", () => {
        let result = {};

        for (let index = 0; index < parsedDataDelivery.length; index++) {
          let currentValueDelivery = parsedDataDelivery[index];
          let bowlingTeam = currentValueDelivery.bowling_team;

          if (matchId.includes(currentValueDelivery.match_id)) {
            if (result[bowlingTeam]) {
              result[bowlingTeam] =
                result[bowlingTeam] + Number(currentValueDelivery.extra_runs);
            } else {
              result[bowlingTeam] = Number(currentValueDelivery.extra_runs);
            }
          }
        }
        const content = JSON.stringify(result);

        fs.writeFile(
          "../public/output/3-extra-runs-conceded-per-team-2016.json",
          content,
          function (err) {
            if (err) {
              console.error(err);
            }
          }
        );
      });
  });
